from . import Cog, CommandErrorFeedback
import discord
from discord.ext import commands

class Fixup(Cog):
    fixup_succeeded_response = {
        "en-GB": "Fixup message successfully sent",
        "fr": "Le message fixup a bien été envoyé",
    }

    def __init__(self, bot: discord.Bot):
        self.bot = bot
        self.replace = {
            "twitter.com": "fxtwitter.com",
            "x.com": "fixupx.com",
            "instagram.com": "ddinstagram.com",
            "reddit.com": "rxddit.com",
        }

    @commands.slash_command(
        name="fixup",
        description="Fixup link to show media on the embedded view",
        description_localizations={
            "fr": "Fixup le lien pour afficher les médias dans la vue intégrée",
        },
        options=[
            discord.Option(
                name = "message",
                input_type=discord.SlashCommandOptionType.string,
                required=True,
                description="your message",
                description_localizations={
                    "fr": "ton message"
                }
            ), discord.Option(
                name="spoiler",
                input_type=discord.SlashCommandOptionType.boolean,
                required=False,
                description="fill the option if the video should be in spoiler",
                description_localizations={
                    "fr": "remplir l'option si la vidéo doit être en spoiler"
                }
            )
        ],
        integration_types=[discord.IntegrationType.guild_install, discord.IntegrationType.user_install],
        contexts=[discord.InteractionContextType.bot_dm, discord.InteractionContextType.guild, discord.InteractionContextType.private_channel],
    )
    async def fixup_message(self, ctx: discord.ApplicationContext, message_text: str, spoiler: bool = False):
        for original, replacement in self.replace.items():
            if message_text.find(original) != -1 and message_text.find(replacement) == -1:
                message_text = message_text.replace(original, replacement, 1)
                if type(ctx.channel) is discord.TextChannel:
                    webhook = await self.get_webhook(ctx)
                    await webhook.send(message_text, username=ctx.author.display_name, avatar_url=ctx.author.display_avatar.url)
                    await ctx.respond(
                        "✅ "+self.fixup_succeeded_response.get(ctx.locale or "en-GB", self.fixup_succeeded_response["en-GB"]),
                        delete_after=5, ephemeral=True
                    )
                else:
                    await ctx.respond(message_text)
                return
                
        raise CommandErrorFeedback({
            "en-GB": "Couldn't find any URL to fixup",
            "fr": "Aucune URL à fixup n'a été trouvée",
        })
        
    async def get_webhook(self, ctx: discord.ApplicationContext):
        webhook_list = await ctx.channel.webhooks()
        webhook = None
        # first find existing webhook
        for w in webhook_list:
            if w.name == "aquassistant" and w.user == self.bot.user:
                webhook = w
                break
        return webhook or await ctx.channel.create_webhook(name="aquassistant")
    