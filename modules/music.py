import asyncio

from . import Cog, CommandErrorFeedback
import discord
from discord.ext import commands
import yt_dlp

ytdl_opts = {
    'format': 'bestaudio/best',
    'extractaudio': True,
    'audioformat': 'mp3',
    'outtmpl': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0',
}

ydl = yt_dlp.YoutubeDL(ytdl_opts)


class Song:
    def __init__(self, url: str):
        self.url = url
        data = ydl.extract_info(url, download=False)
        if "entries" in data:
            # url was a search query
            data = data["entries"][0]
        self.title = data.get("title", "Untitled")
        self.playback_url = data.get("url")
        self.thumbnail_url = data.get("thumbnails", [{}])[-1].get("url", None)

    def get_embed(self, paused: bool = False) -> discord.Embed:
        embed = discord.Embed(title="Music player", type="rich")
        embed.add_field(name="Playback paused" if paused else "Currently playing", value=self.title)
        embed.add_field(name="Source", value=self.url, inline=False)
        if self.thumbnail_url is not None:
            embed.set_thumbnail(url=self.thumbnail_url)
        return embed


class Session:
    def __init__(self, guild_id: int):
        self.guild_id = guild_id
        self._playlist: list[Song] = []
        self.volume: float = 0.2
        self.voice_client: discord.VoiceClient | None = None
        self.message: discord.Message | None = None

    def add_music(self, url: str):
        self._playlist.append(Song(url))

    def next_music(self):
        self._playlist.pop(0)

    def empty_playlist(self):
        self._playlist.clear()

    @property
    def current_song(self) -> Song:
        return self._playlist[0]


class Controls(discord.ui.View):
    def __init__(self, session: Session):
        super().__init__()
        self.session = session
        self.guild_id = session.guild_id

    @discord.ui.button(emoji="⏹", style=discord.ButtonStyle.secondary)
    async def stop_music(self, btn: discord.ui.Button, interaction: discord.Interaction):
        self.session.empty_playlist()
        self.session.voice_client.stop()
        await interaction.response.edit_message()

    @discord.ui.button(emoji="⏯️", style=discord.ButtonStyle.secondary)
    async def play_pause_music(self, btn: discord.ui.Button, interaction: discord.Interaction):
        if self.session.voice_client.is_paused():
            self.session.voice_client.resume()
        else:
            self.session.voice_client.pause()
        await interaction.response.edit_message(
            embed=self.session.current_song.get_embed(paused=self.session.voice_client.is_paused()))

    @discord.ui.button(emoji="⏭️", style=discord.ButtonStyle.secondary)
    async def skip_music(self, btn: discord.ui.Button, interaction: discord.Interaction):
        self.session.voice_client.stop()
        await interaction.response.edit_message()

    @discord.ui.button(emoji="🔉", style=discord.ButtonStyle.secondary)
    async def vol_down_music(self, btn: discord.ui.Button, interaction: discord.Interaction):
        self.session.volume = max(self.session.volume-0.02, 0)
        self.session.voice_client.source.volume = self.session.volume
        await interaction.response.edit_message()

    @discord.ui.button(emoji="🔊", style=discord.ButtonStyle.secondary)
    async def vol_up_music(self, btn: discord.ui.Button, interaction: discord.Interaction):
        self.session.volume = min(self.session.volume + 0.02, 1)
        self.session.voice_client.source.volume = self.session.volume
        await interaction.response.edit_message()


class Music(Cog):
    sessions: dict[int, Session] = {}

    def __init__(self, bot: discord.Bot):
        self.bot = bot

    @commands.slash_command(
        name="play",
        description="Plays or adds a video in the queue",
        description_localizations={
            "fr": "Joue ou ajoute une vidéo dans la file d'attente"
        },
        options=[
            discord.Option(
                name="url_or_search",
                name_localizations={
                    "fr": "url_ou_recherche"
                },
                required=True,
                description="Video URL",
                description_localizations={
                    "fr": "URL de la vidéo"
                }
            )
        ]
    )
    async def play(self, ctx: discord.ApplicationContext, url: str):
        if ctx.user.voice is None or ctx.user.voice.channel is None:
            # User is not in a voice channel
            raise CommandErrorFeedback({
                "en-GB": "You need to be in a voice channel",
                "fr": "Vous devez être dans un salon vocal"
            })
        if ctx.voice_client is not None and ctx.user.voice.channel is not ctx.voice_client.channel:
            # User is not in the bot's voice channel
            raise CommandErrorFeedback({
                "en-GB": "You need to be in the same voice channel as the bot",
                "fr": "Vous devez être dans le même salon vocal que le bot"
            })

        await ctx.defer(ephemeral=True)

        if ctx.guild_id not in self.sessions:
            # Not currently playing, creating the session's information
            self.sessions[ctx.guild_id] = Session(ctx.guild_id)
        session = self.sessions[ctx.guild_id]

        try:
            self.sessions[ctx.guild_id].add_music(url)
        except Exception:
            raise CommandErrorFeedback({
                "en-GB": "The video couldn't be retrieved",
                "fr": "La vidéo n'a pas pu être récupérée"
            })

        if ctx.voice_client is None:
            # Connect to a voice channel and create the player's controls
            session.voice_client = await ctx.user.voice.channel.connect()
            embed = discord.Embed(title="Music player", type="rich")
            embed.add_field(name="Not playing", value="Please wait")
            session.message = await ctx.send(embed=embed,
                                                                 view=Controls(self.sessions[ctx.guild_id]))
            session.voice_client.play(
                self.get_source(session.current_song, session.volume),
                after=lambda _: self.play_another_song(session)
            )
            await session.message.edit(embed=session.current_song.get_embed())

        responses = {
            "en-GB": "Music added to the queue",
            "fr": "Musique ajoutée à la liste d'attente"
        }
        await ctx.respond("✅ "+responses.get(ctx.locale, responses["en-GB"]), ephemeral=True, delete_after=5)

    @staticmethod
    def get_source(song: Song, volume: float) -> discord.PCMVolumeTransformer:
        return discord.PCMVolumeTransformer(discord.FFmpegPCMAudio(song.playback_url), volume)

    def play_another_song(self, session: Session):
        try:
            session.next_music()
            session.voice_client.play(self.get_source(session.current_song, session.volume),
                                      after=lambda _: self.play_another_song(session))
            asyncio.run_coroutine_threadsafe(session.message.edit(embed=session.current_song.get_embed()),
                                             self.bot.loop)
        except IndexError:
            session.voice_client.stop()
            asyncio.run_coroutine_threadsafe(session.message.delete(reason="End of music queue"), self.bot.loop)
            asyncio.run_coroutine_threadsafe(session.voice_client.disconnect(), self.bot.loop)
