import discord
import logging


class CommandErrorFeedback(discord.ApplicationCommandError):
    def __init__(self, responses: dict[str,str]):
        super().__init__()
        self.responses = responses


class Cog(discord.Cog):
    default_error_response = {
        "en-GB": "An unknown error has occurred",
        "fr": "Une erreur inconnue est survenue"
    }

    async def cog_command_error(self, ctx: discord.ApplicationContext, error: Exception) -> None:
        if isinstance(error, CommandErrorFeedback):
            await ctx.respond("⚠️ "+error.responses.get(ctx.locale or "en-GB", error.responses['en-GB'])+" ⚠️",
                              ephemeral=True, delete_after=15)
        else:
            logging.error(f"Unknown error with command {ctx.command.name}: {error}")
            await ctx.respond("⚠️ " + self.default_error_response.get(ctx.locale or "en-GB",
                                                                      self.default_error_response['en-GB']) + " ⚠️",
                              ephemeral=True, delete_after=15)
