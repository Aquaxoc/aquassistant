from . import Cog, CommandErrorFeedback
import discord
from discord.ext import commands
from datetime import datetime
import yt_dlp
import os
import re


class Downloader(Cog):
    download_failed_response = {
        "en-GB": "Download failed",
        "fr": "Le téléchargement a échoué"
    }

    download_succeeded_response = {
        "en-GB": "Download succeed",
        "fr": "Le téléchargement a réussi"
    }

    download_no_format_response = {
        "en-GB": "No format found or video too big (>10MB)",
        "fr": "Aucun format n'a été trouvé, ou la vidéo est trop lourde (>10Mo)"
    }

    def __init__(self, bot: discord.Bot):
        self.bot = bot
        os.makedirs("dl", 0o700, True)

    @commands.slash_command(
        name="download",
        description="Downloads a video and uploads it here",
        description_localizations={
            "fr": "Télécharge une vidéo et l'envoie ici"
        },
        options=[
            discord.Option(
                name="url",
                required=True,
                description="Video URL",
                description_localizations={
                    "fr": "URL de la vidéo"
                }
            ), discord.Option(
                name = "message",
                input_type=discord.SlashCommandOptionType.string,
                required=False,
                description="your message",
                description_localizations={
                    "fr": "ton message"
                }
            ), discord.Option(
                name="spoiler",
                input_type=discord.SlashCommandOptionType.boolean,
                required=False,
                description="fill the option if the video should be in spoiler",
                description_localizations={
                    "fr": "remplir l'option si la vidéo doit être en spoiler"
                }
            )
        ]
    )
    async def slash_download(self, ctx: discord.ApplicationContext, url: str, message_text = "", spoiler: bool = False):
        regex = (r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s("
                 r")<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))")
        if not re.match(regex, url):
            raise CommandErrorFeedback({
                "en-GB": "The given URL is invalid",
                "fr": "L'URL donnée est invalide"
            })
        await ctx.defer(ephemeral=True)
        filepath = None
        try:
            filepath = self.download_video(url, str(ctx.user.id))
            video = discord.File(filepath, spoiler=spoiler)
            embed = discord.Embed(title="Source", type='rich', url=url)
            if type(ctx.channel) is discord.TextChannel:
                webhook_list = await ctx.channel.webhooks()
                webhook = None
                # first find existing webhook
                for w in webhook_list:
                    if w.name == "video downloader" and w.user == self.bot.user:
                        webhook = w
                        break
                if webhook is None:
                    # if not found, create one
                    webhook = await ctx.channel.create_webhook(name="video downloader")
                await webhook.send(message_text, embed=embed, file=video, username=ctx.author.display_name, avatar_url=ctx.author.display_avatar.url)
            elif type(ctx.channel) is discord.DMChannel:
                await ctx.channel.send(embed=embed, file=video)
            await ctx.respond(
                "✅ "+self.download_succeeded_response.get(ctx.locale or "en-GB", self.download_succeeded_response["en-GB"]),
                delete_after=5
            )
        except yt_dlp.DownloadError:
            raise CommandErrorFeedback(self.download_failed_response)
        finally:
            if filepath is not None and os.path.isfile(filepath):
                os.remove(filepath)

    @commands.message_command(
        name="Download video",
        name_localizations={
            "fr": "Télécharger la vidéo"
        }
    )
    async def message_download(self, ctx: discord.ApplicationContext, msg: discord.Message):
        regex = (r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s("
                 r")<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))")
        url = re.findall(regex, msg.content)
        filepath = None
        if len(url) == 0:
            raise CommandErrorFeedback({
                "en-GB": "Couldn't find any link in the message",
                "fr": "Aucun lien n'a été trouvé dans le message"
            })
        await ctx.defer(ephemeral=True)
        try:
            filepath = self.download_video(url[0][0], str(ctx.user.id))
            video = discord.File(filepath)
            await msg.reply(file=video, silent=True)
            await ctx.respond(
                "✅ "+self.download_succeeded_response.get(ctx.locale or "en-GB", self.download_succeeded_response["en-GB"]),
                delete_after=5
            )
        except yt_dlp.DownloadError:
            raise CommandErrorFeedback(self.download_failed_response)
        finally:
            if filepath is not None and os.path.isfile(filepath):
                os.remove(filepath)

    @staticmethod
    def download_with_format(url: str, formats: str, filepath: str):
        download_opts = {
            "quiet": True,
            "no_warnings": True,
            "format": formats,
            "outtmpl": filepath,
            'postprocessors': [{ # Never tested it lol
                'key': 'FFmpegVideoConvertor',
                'preferedformat': 'mp4',
            }],
        }
        yt_dlp.YoutubeDL(download_opts).download(url)

    @staticmethod
    def get_best_format(url: str):
        options = {
            "quiet": True,
            "no_warnings": True,
            "simulate": True,
        }

        infos = yt_dlp.YoutubeDL(options).extract_info(url, download=False)
        if infos is None:
            raise Exception("No infos")
        duration = infos["duration"]
        if duration is None:
            raise Exception("No duration")
        formats = (infos or {}).get("formats", {})

        audio_formats = []
        video_formats = []
        full_formats = []

        for f in reversed(formats):
            if "tbr" not in f or "vcodec" not in f or "acodec" not in f:
                continue
            if f["vcodec"] == "none":
                audio_formats.append(f)
            elif f["acodec"] == "none":
                video_formats.append(f)
            else:
                full_formats.append(f)

        # first try to take the best full format
        if len(full_formats) > 0:
            for f in full_formats:
                # print(f"Format {f["format_id"]}:\n\t{f}")
                best_size = f["tbr"] * duration
                # print(f"Format {f["format_id"]}: {best_size}")
                if best_size < 8*10*1024:
                    return f["format_id"]

        # then try to assemble video+audio
        if len(audio_formats) > 0 and len(video_formats) > 0:
            for af in audio_formats:
                for vf in video_formats:
                    best_size = (af["abr"] + vf["vbr"]) * duration
                    if best_size < 8*10*1024:
                        return f"{vf['format_id']}+{af['format_id']}"

        raise CommandErrorFeedback(Downloader.download_no_format_response)

    @staticmethod
    def download_video(url: str, name: str) -> str:
        filepath = f"dl/{name}-{datetime.now().timestamp()}"
        format = Downloader.get_best_format(url)
        Downloader.download_with_format(url, format, filepath)
        return filepath+".mp4"
