import asyncio

from . import Cog, CommandErrorFeedback
import aiohttp
import discord
from discord.ext import commands


class Cat(Cog):
    def __init__(self, bot: discord.Bot):
        self.bot = bot

    @commands.slash_command(
        name="cat",
        description="Sends a random cat",
        description_localizations={
            "fr": "Envoie un chat aléatoire"
        },
        integration_types=[discord.IntegrationType.guild_install, discord.IntegrationType.user_install],
        contexts=[discord.InteractionContextType.bot_dm, discord.InteractionContextType.guild, discord.InteractionContextType.private_channel],
    )
    async def cat(self, ctx: discord.ApplicationContext):
        await ctx.defer()
        try:
            async with aiohttp.ClientSession() as session:
                async with session.get("https://api.thecatapi.com/v1/images/search") as req:
                    if not req.ok:
                        raise Exception
                    result = await req.json()
                    if 'url' in result[0]:
                        await ctx.respond(result[0]['url'])
        except Exception:
            raise CommandErrorFeedback({
                "en-GB": "Failed to find a cat",
                "fr": "Erreur lors de la recherche de chat"
            })
