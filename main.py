import discord
import config
from modules import fixup, cat, dog, music, fixup


if __name__ == '__main__':
    bot = discord.Bot(intents=discord.Intents().all())
    bot.add_cog(fixup.Fixup(bot))
    bot.add_cog(cat.Cat(bot))
    bot.add_cog(dog.Dog(bot))
    bot.add_cog(music.Music(bot))
    bot.run(config.token)
